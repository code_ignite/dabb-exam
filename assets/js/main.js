jQuery(document).ready(function($) {

  //==================================================
  // Дропдаун списка городов
  //==================================================
  (function() {
    $('.guide-city-selector__dropdown-link').on( 'click', function(e) {
      e.stopPropagation();
      $('.guide-city-selector__dropdown-block').toggleClass('guide-city-selector__dropdown-block_show');
    });

    $(document).on( 'click', function() {
      $('.guide-city-selector__dropdown-block').removeClass('guide-city-selector__dropdown-block_show');
    });
  })();



  //==================================================
  // Раскрываем карточку места
  //==================================================
  (function() {
    var item = $('.guide-places__item'),
        card = $('.guide-places-card'),
        closeBtn = $('.guide-places-card__close'),
        itemObj = {},
        itemsArray = [];

    $(item).each( function( index, el ) {
      itemObj = {
        attrValue: $(el).attr('data-guide-places-item'),
        itemOffsetTop: el.offsetTop
      }

      itemsArray.push(itemObj);
    });

    $(item).on( 'click', function(e) {
      var itemThis = $(this),
          itemThisDataAttr = $(itemThis).attr('data-guide-places-item'),
          selectedCard = $('[data-guide-places-card=' + itemThisDataAttr + ']');

      e.stopPropagation();

      $(itemThis).toggleClass('guide-places__item_margin-bottom');
      $(selectedCard).toggleClass('guide-places-card_show');
      $(selectedCard).css('top', itemsArray[itemThisDataAttr - 1].itemOffsetTop + 380 + 'px');

      if ( $(item).hasClass('guide-places__item_margin-bottom') || $(card).hasClass('guide-places-card_show') ) {
        $(item).removeClass('guide-places__item_margin-bottom');
        $(card).removeClass('guide-places-card_show');
        $(card).css('top', '');
        $(itemThis).toggleClass('guide-places__item_margin-bottom');
        $(selectedCard).toggleClass('guide-places-card_show');
        $(selectedCard).css('top', itemsArray[itemThisDataAttr - 1].itemOffsetTop + 380 + 'px');
      }
    });

    $(closeBtn).on( 'click', function() {
      $(item).removeClass('guide-places__item_margin-bottom');
      $(card).removeClass('guide-places-card_show');
      $(card).css('top', '');
    });
  })();
});
